package querybatches_test

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"

	"bitbucket.org/classroomsystems/batches"
	"bitbucket.org/classroomsystems/querybatches"

	_ "github.com/mattn/go-sqlite3"
)

var DB *sql.DB

type record1 struct {
	ID     int64
	Value1 int64
}

type record2 struct {
	ID     int64
	Value2 int64
}

type record3 struct {
	ID     int64
	T1ID   int64
	Value3 int64
}

// This example collates records
// from two tables that share an ID space.
// Note that only one  query is active at a time,
// even though processing is interleaved,
// so this can be done on a single
// database connection or within a transaction.
func Example_collate() {
	t1 := querybatches.New(DB,
		`select {{.}} from t1
			where ID > {{$last.ID}}
			order by ID
			limit {{$batchSize}}`,
		record1{},
	)
	t1.SetDoneValue(record1{ID: maxInt64})

	t2 := querybatches.New(DB,
		`select {{.}} from t2
			where ID > {{$last.ID}}
			order by ID
			limit {{$batchSize}}`,
		record2{},
	)
	t2.SetDoneValue(record2{ID: maxInt64})

	var r1 record1
	var r2 record2
	t1.Next(&r1)
	t2.Next(&r2)

	var inBoth, onlyInT1, onlyInT2 int
	for !t1.Done() || !t2.Done() {
		id := min(r1.ID, r2.ID)
		if id == r1.ID && id == r2.ID {
			// do some processing
			inBoth++
			t1.Next(&r1)
			t2.Next(&r2)
		} else if id == r1.ID {
			// do some processing
			onlyInT1++
			t1.Next(&r1)
		} else {
			// do some processing
			onlyInT2++
			t2.Next(&r2)
		}
	}

	if err := batches.AnyErr(t1, t2); err != nil {
		log.Fatalln(err)
	}
	fmt.Println("record counts:", onlyInT1, inBoth, onlyInT2)
	// Output:
	// record counts: 5977 6042 5938
}

// This example processes records
// from two tables with a parent/child relationship.
// Each record in t1 may have zero or more
// associated records in t3.
// The two queries below
// perform the equivalent
// of an SQL FULL JOIN of t1 and t3.
// However, if the join were done in SQL,
// the data structures would need to be modified
// to accept NULL values,
// and each row in t1 would be repeated
// for every child row in t3.
// Retrieving the data with two queries
// avoids these complications.
func Example_parentChild() {
	t1 := querybatches.New(DB,
		`select {{.}} from t1
			where ID > {{$last.ID}}
			order by ID
			limit {{$batchSize}}`,
		record1{},
	)
	t1.SetDoneValue(record1{ID: maxInt64})

	t3 := querybatches.New(DB,
		`select {{.}} from t3
			where T1ID > {{$last.T1ID}}
				or T1ID = {{$last.T1ID}} and ID > {{$last.ID}}
			order by T1ID, ID
			limit {{$batchSize}}`,
		record3{},
	)
	t3.SetDoneValue(record3{ID: maxInt64, T1ID: maxInt64})

	var r1 record1
	var r3 record3
	t1.Next(&r1)
	t3.Next(&r3)

	var parents, children, orphans int
	for !t1.Done() || !t3.Done() {
		id := min(r1.ID, r3.T1ID)
		for r3.T1ID == id {
			if r1.ID == id {
				// Process one of r1's children.
				children++
			} else {
				// Process an orphan.
				orphans++
			}
			t3.Next(&r3)
		}
		if r1.ID == id {
			// Process a parent record.
			parents++
			t1.Next(&r1)
		}
	}

	if err := batches.AnyErr(t1, t3); err != nil {
		log.Fatalln(err)
	}
	fmt.Println("record counts:", parents, children, orphans)
	// Output:
	// record counts: 12019 20058 9940
}

func init() {
	// Set the batch size low to force multiple batches.
	querybatches.DefaultBatchSize = 1000

	var err error
	DB, err = sql.Open("sqlite3", ":memory:")
	if err != nil {
		log.Fatalln(err)
	}
	tx, err := DB.Begin()
	if err != nil {
		log.Fatalln(err)
	}
	defer tx.Commit()
	x := &errExec{x: tx}
	x.Exec("create table t1 (ID integer primary key, Value1 integer)")
	x.Exec("create table t2 (ID integer primary key, Value2 integer)")
	x.Exec("create table t3 (ID integer primary key, T1ID integer, Value3 integer)")
	rand.Seed(0) // The test needs to be reproducible.
	t3id := 1
	for id := 1; id < 30000 && x.err == nil; id++ {
		switch rand.Intn(5) {
		case 0:
			// no record
		case 1:
			x.Exec("insert into t1 (ID, Value1) values (?, ?)", id, rand.Int31())
			for ; t3id < id; t3id++ {
				x.Exec("insert into t3 (ID, T1ID, Value3) values (?, ?, ?)", t3id, id, rand.Int31())
			}
		case 2:
			x.Exec("insert into t2 (ID, Value2) values (?, ?)", id, rand.Int31())
		case 3:
			x.Exec("insert into t1 (ID, Value1) values (?, ?)", id, rand.Int31())
			x.Exec("insert into t2 (ID, Value2) values (?, ?)", id, rand.Int31())
			for ; t3id < id; t3id++ {
				x.Exec("insert into t3 (ID, T1ID, Value3) values (?, ?, ?)", t3id, id, rand.Int31())
			}
		case 4:
			// orphans
			for ; t3id < id; t3id++ {
				x.Exec("insert into t3 (ID, T1ID, Value3) values (?, ?, ?)", t3id, id, rand.Int31())
			}
		}
	}
	if x.err != nil {
		log.Fatalln(err)
	}
}

type errExec struct {
	x interface {
		Exec(query string, args ...interface{}) (sql.Result, error)
	}
	err error
}

func (e *errExec) Exec(query string, args ...interface{}) {
	if e.err != nil {
		return
	}
	_, e.err = e.x.Exec(query, args...)
}

const maxInt64 = 9223372036854775807

func min(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}
