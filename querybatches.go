/*
Package querybatches enables simple creation of
batches.Seq sequences for database queries using the
sqldata package. With these sequences, processing code
can be written as if records are loaded one-at-a-time
from any number of simultaneous queries, but the
records are actually loaded in batches of a specified
size, balancing memory requirements and database
latency.

Query templates used with this package
may refer to two preset variables:

	$batchSize
		The maximum number of records to be returned by a
		single query. It should be used in a LIMIT or similar
		clause in the query.
	$last
		The scanned value of the last row returned in the
		previous batch. If this is the first batch, it is
		the dstType value as passed to New or NewPrepared.
		$last is primarily intended for keyset paging.
*/
package querybatches

import (
	"reflect"
	"runtime"

	"bitbucket.org/classroomsystems/batches"
	"bitbucket.org/classroomsystems/sqldata"
)

// DefaultBatchSize is the default number of records loaded per database query.
// It can be overridden for a given batch using SetBatchSize.
var DefaultBatchSize = 100000

type hiddenArgs struct {
	// Size is the batch size. It should be used in a LIMIT
	// or similar clause in the query.
	batchSize int

	// Last is the scanned value of the last row returned
	// by the previous batch. If this is the first batch,
	// it is the dstType value passed to New or NewPrepared.
	last interface{}
}

// New returns a new batches.Seq
// which will run query on db
// for each batch of records to be loaded.
//
// The query is parsed for each execution,
// so it may use any construct
// in sqldata's template language.
func New(db sqldata.Queryer, query string, dstType interface{}, args ...interface{}) *batches.Seq {
	_, file, line, _ := runtime.Caller(1)
	query = "{{$batchSize := shift}}{{$last := shift}}" + query
	hiddenArgs, realArgs := makeArgs(dstType, args)
	return batches.New(&queryBatches{
		db:         db,
		query:      query,
		file:       file,
		line:       line,
		hiddenArgs: hiddenArgs,
		args:       realArgs,
	})
}

// New returns a new batches.Seq
// which will run query on db
// for each batch of records to be loaded.
//
// The query is parsed and prepared once,
// so it may only use constructs
// in sqldata's template language
// that are appropriate for prepared statements.
// The prepared statement is closed
// when the sequence is closed.
//
// This function creates a single sequence
// that does not re-parse the query
// every time a batch is loaded.
// To prepare a statement for use by many sequences, see Prepare.
func NewPrepared(db sqldata.Queryer, query string, dstType interface{}, args ...interface{}) *batches.Seq {
	query = "{{$batchSize := shift}}{{$last := shift}}" + query
	hiddenArgs, realArgs := makeArgs(dstType, args)
	stmt, err := sqldata.Prepare(db, query, dstType, realArgs...)
	return batches.New(&queryBatches{
		stmt:       stmt,
		err:        sqldata.RelocateError(err),
		hiddenArgs: hiddenArgs,
		args:       realArgs,
	})
}

// A Stmt is a prepared statement from which
// any number of QueryBatches sequences may be created.
// It wraps a sqldata.Stmt.
// A Stmt is safe for use by multiple goroutines.
type Stmt struct {
	dstType interface{}
	stmt    *sqldata.Stmt
}

// Prepare creates a Stmt by preparing the given query
// on db with sqldata's DefaultDialect.
func Prepare(db sqldata.Queryer, query string, dstType interface{}, argTypes ...interface{}) (*Stmt, error) {
	s, err := PrepareDialect(sqldata.DefaultDialect, db, query, dstType, argTypes...)
	return s, sqldata.RelocateError(err)
}

// PrepareDialect creates a Stmt by preparing query
// on db with Dialect d.
func PrepareDialect(d *sqldata.Dialect, db sqldata.Queryer, query string, dstType interface{}, argTypes ...interface{}) (*Stmt, error) {
	query = "{{$batchSize := shift}}{{$last := shift}}" + query
	_, realArgs := makeArgs(dstType, argTypes)
	stmt, err := d.Prepare(db, query, dstType, realArgs...)
	return &Stmt{dstType, stmt}, sqldata.RelocateError(err)
}

// PrepareAtInit returns a new Stmt from query
// which will be prepared when atInit is initialized.
func PrepareAtInit(atInit *sqldata.AtInit, query string, dstType interface{}, argTypes ...interface{}) *Stmt {
	_, file, line, _ := runtime.Caller(1)
	query = "{{$batchSize := shift}}{{$last := shift}}" + query
	_, realArgs := makeArgs(dstType, argTypes)
	s := &Stmt{dstType, nil}
	atInit.Do(func(db sqldata.Queryer, d *sqldata.Dialect) error {
		var err error
		s.stmt, err = d.Prepare(db, query, dstType, realArgs...)
		return sqldata.RelocateErrorTo(err, file, line)
	})
	return s
}

// New returns a new QueryBatches which will execute
// the prepared statement with the given args.
func (s *Stmt) New(args ...interface{}) *batches.Seq {
	hiddenArgs, realArgs := makeArgs(s.dstType, args)
	return batches.New(&queryBatches{
		stmt:       s.stmt,
		hiddenArgs: hiddenArgs,
		args:       realArgs,
	})
}

// Close closes the underlying sqldata.Stmt.
func (s *Stmt) Close() error {
	return s.stmt.Close()
}

func makeArgs(dstType interface{}, args []interface{}) (*hiddenArgs, []interface{}) {
	h := &hiddenArgs{
		batchSize: DefaultBatchSize,
		last:      dstType,
	}
	a := make([]interface{}, len(args)+2)
	copy(a[2:], args)
	a[0], a[1] = h.batchSize, h.last
	return h, a
}

// SetBatchSize changes the value of $batchSize
// passed to queries in sequence s.
// It may be called at any time
// to change the value for future queries.
// If s is not a sequence created by this package,
// SetBatchSize will panic.
func SetBatchSize(s *batches.Seq, size int) {
	s.Loader().(*queryBatches).hiddenArgs.batchSize = size
}

type queryBatches struct {
	// Used via New
	db    sqldata.Queryer
	query string
	file  string
	line  int

	// Used via NewPrepared or Prepare
	stmt *sqldata.Stmt
	err  error

	// Used in any case
	hiddenArgs *hiddenArgs
	args       []interface{}
}

var _ batches.LoadCloser = &queryBatches{}

func (b *queryBatches) LoadBatch(dst interface{}) error {
	if b.err != nil {
		return b.err
	}
	b.args[0] = b.hiddenArgs.batchSize
	b.args[1] = b.hiddenArgs.last
	if b.stmt == nil {
		b.err = sqldata.QueryAll(b.db, b.query, dst, b.args...)
		b.err = sqldata.RelocateErrorTo(b.err, b.file, b.line)
	} else {
		b.err = b.stmt.QueryAll(b.args...).Scan(dst)
	}
	if b.err != nil {
		return b.err
	}
	v := reflect.ValueOf(dst).Elem() // dst must be a pointer to a slice.
	if v.Len() > 0 {
		b.hiddenArgs.last = v.Index(v.Len() - 1).Interface()
	}
	return nil
}

func (b *queryBatches) Close() error {
	if b.stmt == nil {
		return nil
	}
	return b.stmt.Close()
}
