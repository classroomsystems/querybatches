package querybatches

import (
	"database/sql"
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/classroomsystems/sqldata"

	_ "github.com/mattn/go-sqlite3"
)

func TestLocatedErrors(t *testing.T) {
	const badTemplate = "{{else}}"
	var errs [5]error
	var dst int
	atInit := new(sqldata.AtInit)
	q1 := New(errorQueryer{}, badTemplate, &dst)
	q2 := NewPrepared(errorQueryer{}, badTemplate, &dst)
	_ = PrepareAtInit(atInit, badTemplate, &dst)
	q1.Next(&dst)
	q2.Next(&dst)
	errs[0] = q1.Err()
	errs[1] = q2.Err()
	errs[2] = atInit.Init(errorQueryer{})
	_, errs[3] = Prepare(errorQueryer{}, badTemplate, &dst)
	_, errs[4] = PrepareDialect(sqldata.DefaultDialect, errorQueryer{}, badTemplate, &dst)
	var baseline int
	for i := range errs {
		if errs[0] == nil {
			t.Error("missing error", i)
			continue
		}
		file, line := sqldata.ErrorLocation(errs[i])
		if !strings.HasSuffix(file, "/querybatches_test.go") {
			t.Error("unexpected file", i, file, "for error", errs[i])
		}
		offset := 0
		switch i {
		case 0:
			baseline = line // q1 := New(...)
		case 1, 2:
			// NewPrepared and PrepareAtInit are before the main sequence
		default:
			offset = 5 // baseline is 5 lines before the main sequence.
		}
		if line != baseline+offset+i {
			t.Error("unexpected error line", i, "got", line, "expecting", baseline+offset+i)
		}
	}
}

type errorQueryer struct{}

var _ sqldata.Queryer = errorQueryer{}

func (errorQueryer) Exec(query string, args ...interface{}) (sql.Result, error) {
	return nil, fmt.Errorf("errorQueryer")
}
func (errorQueryer) Prepare(query string) (*sql.Stmt, error) {
	return nil, fmt.Errorf("errorQueryer")
}
func (errorQueryer) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return nil, fmt.Errorf("errorQueryer")
}
